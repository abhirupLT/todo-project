const URL = `http://api.zippopotam.us/IN/`;

export const getPincodeDetailsService = async (pincode) => {
  const response = await fetch(`${URL}${pincode}`);
  if (!response.ok) {
    throw new Error(response.statusText);
  }
  const data = await response.json();
  return data;
};
