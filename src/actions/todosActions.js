export const TodosActionTypes = {
  ADD_TODO: 'add todo',
  DELETE_TODO: 'delete todo',
  EDIT_TODO: 'edit todo',
  TOGGLE_COMPLETION_STATUS_OF_TODO: 'toggle completion status of todo',
};

/**
 * TodoActions handles actions relatd to todos data
 */

class TodoActions {
  addTodo(item) {
    return {
      type: TodosActionTypes.ADD_TODO,
      item,
    };
  }

  deleteTodo(id) {
    return {
      type: TodosActionTypes.DELETE_TODO,
      id,
    };
  }

  editTodo(item) {
    return { type: TodosActionTypes.EDIT_TODO, item };
  }

  toggleCompletionStatusOfTodo(id) {
    return {
      type: TodosActionTypes.TOGGLE_COMPLETION_STATUS_OF_TODO,
      id,
    };
  }
}

export default new TodoActions();
