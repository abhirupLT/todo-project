export const PincodeActionTypes = {
  LOAD_PINCODEDETAILS: 'load pincodedetails',
  SET_NEXT_PINCODE: 'set next pincode',
  LOAD_PINCODEDETAILS_SUCCESS: 'load pincodedetails success',
  LOAD_PINCODEDETAILS_ERROR: 'load pincodedetails error',
};

/**
 * PincodeActions handles actions relatd to pincode data
 */

class PincodeActions {
  loadPincodeDetails() {
    return {
      type: PincodeActionTypes.LOAD_PINCODEDETAILS,
    };
  }

  setNextPincode(pincode) {
    return {
      type: PincodeActionTypes.SET_NEXT_PINCODE,
      payload: pincode,
    };
  }

  loadPincodeDetailsSuccess(places) {
    return {
      type: PincodeActionTypes.LOAD_PINCODEDETAILS_SUCCESS,
      payload: places,
    };
  }

  loadPincodeDetailsError() {
    return {
      type: PincodeActionTypes.LOAD_PINCODEDETAILS_ERROR,
    };
  }
}

export default new PincodeActions();
