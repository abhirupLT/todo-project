import { combineReducers } from "redux";
import todosReducer from "./todosReducer";
import pincodeReducer from "./pincodeReducer";
import nextPincodeReducer from "./nextPincodeReducer";

export default combineReducers({
  todosReducer,
  pincodeReducer,
  nextPincodeReducer,
});
