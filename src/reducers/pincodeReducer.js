import { PincodeActionTypes } from '../actions/pincodeActions';
/**
 * pincodeReducer  changes state based on pincodeActions
 *
 * @param  {object} state state from the store in redux
 * @param  {object} action     action Object passed from the action to reducer
 * @return {object}            changed/ updated state
 */
export default function pincodeReducer(state = {}, action) {
  let newState = { ...state };
  switch (action.type) {
    case PincodeActionTypes.LOAD_PINCODEDETAILS: {
      newState = { ...newState, isLoading: true, payload: null, error: null };
      break;
    }
    case PincodeActionTypes.LOAD_PINCODEDETAILS_SUCCESS: {
      newState = {
        ...newState,
        isLoading: false,
        payload: action.payload,
        error: null,
      };
      break;
    }
    case PincodeActionTypes.LOAD_PINCODEDETAILS_ERROR: {
      newState = {
        ...newState,
        isLoading: false,
        payload: null,
        error: 'Enter a correct format of Pincode: 110001 to 855126',
      };
      break;
    }
    default:
      break;
  }
  return newState;
}
