import { PincodeActionTypes } from '../actions/pincodeActions';
/**
 * nextPincodeReducer changes state based on pincodeActions
 *
 * @param  {object} state state from the store in redux
 * @param  {object} action     action Object passed from the action to reducer
 * @return {object}            changed/ updated state
 */

export default function nextPincodeReducer(state = '', action) {
  let newState = state;
  switch (action.type) {
    case PincodeActionTypes.SET_NEXT_PINCODE: {
      newState = action.payload;
      break;
    }
    default:
      break;
  }
  return newState;
}
