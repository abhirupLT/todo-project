import { TodosActionTypes } from '../actions/todosActions';
/**
 * todosReducer  changes state based on todosActions
 *
 * @param  {object} state state from the store in redux
 * @param  {object} action     action Object passed from the action to reducer
 * @return {object}            changed/ updated state
 */
export default function todosReducer(state = [], action) {
  let newstate = [...state];
  switch (action.type) {
    case TodosActionTypes.ADD_TODO: {
      const newTodo = { ...action.item, isCompleted: false, id: state.length };
      newstate = [...newstate, newTodo];
      break;
    }
    case TodosActionTypes.DELETE_TODO: {
      newstate = newstate.filter((item) => item.id !== action.id);
      break;
    }
    case TodosActionTypes.EDIT_TODO: {
      newstate = newstate.map((item) => {
        if (item.id === action.item.id) {
          item = action.item;
        }
        return item;
      });
      break;
    }
    case TodosActionTypes.TOGGLE_COMPLETION_STATUS_OF_TODO: {
      newstate = newstate.map((item) => {
        if (item.id === action.id) {
          item.isCompleted = !item.isCompleted;
        }
        return item;
      });
      break;
    }
    default: {
      break;
    }
  }
  return newstate;
}
