import { put, call, takeLatest, select } from 'redux-saga/effects';
import { getPincodeDetailsService } from '../services/pincodeService';
import PincodeActions, { PincodeActionTypes } from '../actions/pincodeActions';

const getPincode = (state) => state.nextPincodeReducer;
/**
 * fetchPincodeDetails helper function for getPincodeDetails
 *
 */
function* fetchPincodeDetails() {
  try {
    const nextPincode = yield select(getPincode);
    const data = yield call(getPincodeDetailsService, nextPincode);
    yield put(PincodeActions.loadPincodeDetailsSuccess(data.places));
  } catch (e) {
    yield put(PincodeActions.loadPincodeDetailsError());
  }
}
/**
 * getPincodeDetails generator function
 *
 */
export default function* getPincodeDetails() {
  yield takeLatest(PincodeActionTypes.LOAD_PINCODEDETAILS, fetchPincodeDetails);
}
