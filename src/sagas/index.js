import { all } from "redux-saga/effects";
import getPincodeDetails from "./pincodesaga";

export default function* rootSaga() {
  yield all([getPincodeDetails()]);
}
