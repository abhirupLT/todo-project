import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import '../styles/navbar.css';

export default class Navbar extends Component {
  render() {
    return (
      <ul className="nav">
        <NavLink to="/" activeClassName="selected" className="link" exact>
          <div>Home</div>
        </NavLink>
        <NavLink to="/add" className="link" exact activeClassName="selected">
          <div>Add</div>
        </NavLink>
        <NavLink
          to="/completed"
          className="link"
          exact
          activeClassName="selected"
        >
          <div>Completed</div>
        </NavLink>
        <NavLink
          to="/pincode"
          className="link"
          exact
          activeClassName="selected"
        >
          <div>Pincode Api</div>
        </NavLink>
      </ul>
    );
  }
}
