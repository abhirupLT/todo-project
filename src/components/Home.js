import React, { Component } from 'react';
import Item from './Item';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import TodoActions from '../actions/todosActions';
class Home extends Component {
  /**
   * renders Home
   *
   * @return {Element} react element
   */
  render() {
    const { items, deleteTodo, toggleCompletionStatusOfTodo } = this.props;
    return (
      <div>
        <h2>React app to add, update or delete item using Route</h2>
        {items.map((item, index) => (
          <Item
            item={item}
            key={index}
            deleteTodo={(id) => deleteTodo(id)}
            toggleCompletionStatusOfTodo={(id) =>
              toggleCompletionStatusOfTodo(id)
            }
            history={this.props.history}
          />
        ))}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    items: state.todosReducer,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    deleteTodo: (id) => dispatch(TodoActions.deleteTodo(id)),
    toggleCompletionStatusOfTodo: (id) =>
      dispatch(TodoActions.toggleCompletionStatusOfTodo(id)),
  };
};

Home.propTypes = {
  items: PropTypes.array,
  deleteTodo: PropTypes.func,
  toggleCompletionStatusOfTodo: PropTypes.func,
  history: PropTypes.object,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
