import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

class Completed extends Component {
  /**
   * renders Completed
   *
   * @return {Element} react element
   */
  render() {
    const { todos } = this.props;
    return (
      <div>
        <h3>Completed Todos</h3>
        {todos.map((todo) => (
          <p key={todo.id}>{todo.name}</p>
        ))}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    todos: state.todosReducer.filter((todo) => todo.isCompleted),
  };
};

Completed.propTypes = {
  todos: PropTypes.array,
};

export default connect(mapStateToProps)(Completed);
