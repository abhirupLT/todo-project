import React, { Component } from 'react';
import { connect } from 'react-redux';
import PincodePlaceDetails from './PincodePlaceDetails';
import '../styles/PincodeDetails.css';
import PropTypes from 'prop-types';

class PincodeDetails extends Component {
  /**
   * renders PincodeDetails
   *
   * @return {Element} react element
   */
  render() {
    const {
      pincodeReducer: { payload },
    } = this.props;
    return (
      <div className="PincodeDetails">
        <table className="PincodeDetails-table">
          <thead>
            <tr>
              <th>State</th>
              <th>Area </th>
              <th>Latitude</th>
              <th>Longitude</th>
            </tr>
          </thead>
          <tbody>
            {payload.map((place, index) => (
              <PincodePlaceDetails key={index} place={place} />
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = ({ pincodeReducer }) => ({
  pincodeReducer,
});

PincodeDetails.propTypes = {
  pincodeReducer: PropTypes.object,
};

export default connect(mapStateToProps, null)(PincodeDetails);
