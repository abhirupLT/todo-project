import React from 'react';
import '../styles/Pincode.css';
import { connect } from 'react-redux';
import PincodeDetails from './PincodeDetails';
import PincodeActions from '../actions/pincodeActions';
import PropTypes from 'prop-types';
import pincodeActions from '../actions/pincodeActions';

class Pincode extends React.Component {
  /**
   * constructor Pincode
   *
   * @param {object} props for the comp
   */
  constructor(props) {
    super(props);
    this.state = {
      pincode: '',
    };
  }
  /**
   * handleChange handle change of form elements
   * @param {object} e event object
   */
  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  handleSubmit = () => {
    this.props.setNextPincode(this.state.pincode);
    this.props.loadPincodeDetails();
  };

  /**
   * renders Pincode
   *
   * @return {Element} react element
   */
  render() {
    const { isLoading, error, payload } = this.props.pincodeReducer;
    return (
      <div>
        <h3>Get pincode location info (Currently available in India)</h3>
        <div className="pincodeForm">
          <div className="form-group">
            <input
              type="number"
              placeholder="Enter a Pincode"
              name="pincode"
              onChange={this.handleChange}
            />
          </div>
          <button type="submit" className="btn" onClick={this.handleSubmit}>
            {isLoading ? 'Loading' : 'Fetch details'}
          </button>
          {error && (
            <div
              style={{
                textAlign: 'center',
                backgroundColor: 'pink',
                padding: '0.3rem',
                margin: '1rem',
              }}
            >
              {error}
            </div>
          )}
        </div>
        {payload && <PincodeDetails />}
        <p style={{ margin: '10px 0' }}>
          Credit{' '}
          <a
            target="_blank"
            href="https://api.zippopotam.us"
            style={{ color: 'red' }}
            rel="noopener noreferrer"
          >
            Zippopotamous
          </a>{' '}
          for the awesome api!
        </p>
      </div>
    );
  }
}

const mapStateToProps = ({ pincodeReducer }) => ({
  pincodeReducer,
});

const mapDispatchToProps = (dispatch) => ({
  loadPincodeDetails: () => dispatch(PincodeActions.loadPincodeDetails()),
  setNextPincode: (pincode) => dispatch(pincodeActions.setNextPincode(pincode)),
});
Pincode.propTypes = {
  pincodeReducer: PropTypes.object,
  setNextPincode: PropTypes.func,
  loadPincodeDetails: PropTypes.func,
};
export default connect(mapStateToProps, mapDispatchToProps)(Pincode);
