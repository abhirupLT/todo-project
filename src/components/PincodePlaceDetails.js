import React, { Component } from 'react';
import PropTypes from 'prop-types';

class PincodePlaceDetails extends Component {
  /**
   * renders PincodePlaceDetails
   *
   * @return {Element} react element
   */
  render() {
    const {
      place: { ['place name']: placename, state, latitude, longitude },
    } = this.props;
    return (
      <tr>
        <td>{state}</td>
        <td>{placename}</td>
        <td>{latitude}</td>
        <td>{longitude}</td>
      </tr>
    );
  }
}

PincodePlaceDetails.propTypes = {
  place: PropTypes.object,
};

export default PincodePlaceDetails;
