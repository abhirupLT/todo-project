import React, { Component } from 'react';
import '../styles/item.css';
import PropTypes from 'prop-types';

export default class Item extends Component {
  /**
   * renders Item
   *
   * @return {Element} react element
   */
  render() {
    const {
      item: { name, isCompleted, id },
      deleteTodo,
      toggleCompletionStatusOfTodo,
      history,
    } = this.props;
    const status = isCompleted ? 'Completed' : 'Not Completed';
    return (
      <div className="items">
        <h4>{name}</h4>
        <button className="btn" onClick={() => deleteTodo(id)}>
          Delete
        </button>
        <button className="btn" onClick={() => history.push(`/edit/${id}`)}>
          Edit
        </button>
        <button
          className="btn"
          onClick={() => toggleCompletionStatusOfTodo(id)}
        >
          {' '}
          {status}
        </button>
      </div>
    );
  }
}

Item.propTypes = {
  item: PropTypes.object,
  deleteTodo: PropTypes.func,
  toggleCompletionStatusOfTodo: PropTypes.func,
  history: PropTypes.object,
};
