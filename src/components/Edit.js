import React, { Component } from 'react';
import '../styles/Edit.css';
import { connect } from 'react-redux';
import TodoActions from '../actions/todosActions';
import PropTypes from 'prop-types';

class Edit extends Component {
  constructor(props) {
    super(props);
    const {
      item: { name, isCompleted },
    } = this.props;
    this.state = {
      itemname: name,
      isCompleted: isCompleted,
    };
  }

  handleInput = (e) => {
    const value =
      e.target.name === 'isCompleted' ? e.target.checked : e.target.value;
    this.setState({ [e.target.name]: value });
  };

  handleSubmit = (e) => {
    if (this.state.itemname === '') {
      alert('Empty Todo');
      return;
    }
    //To be saved to store
    const {
      item: { id },
      editItem,
      history,
    } = this.props;
    const editItemObj = {
      isCompleted: this.state.isCompleted,
      name: this.state.itemname,
      id: id,
    };
    editItem(editItemObj);
    history.push('/');
    e.preventDefault();
  };

  render() {
    return (
      <div>
        <form className="editForm">
          <div className="form-group">
            <input
              type="text"
              name="itemname"
              value={this.state.itemname}
              onChange={this.handleInput}
            />
          </div>
          <div className="form-group">
            <input
              type="checkbox"
              name="isCompleted"
              checked={this.state.isCompleted}
              onChange={this.handleInput}
            />{' '}
            Completed
          </div>
          <button
            type="button"
            className="btn"
            onClick={() => this.props.history.push('/')}
          >
            Cancel{' '}
          </button>
          <button type="button" className="btn" onClick={this.handleSubmit}>
            Submit{' '}
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const {
    match: {
      params: { id },
    },
  } = ownProps;
  return {
    item: state.todosReducer.find((todo) => todo.id === parseInt(id)),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    editItem: (item) => dispatch(TodoActions.editTodo(item)),
  };
};
Edit.propTypes = {
  item: PropTypes.object,
  editItem: PropTypes.func,
  history: PropTypes.object,
};
export default connect(mapStateToProps, mapDispatchToProps)(Edit);
