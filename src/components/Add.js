import React, { Component } from 'react';
import '../styles/Add.css';
import { connect } from 'react-redux';
import TodoActions from '../actions/todosActions';
import PropTypes from 'prop-types';

class Add extends Component {
  /**
   * constructor Add
   *
   * @param {object} props for the comp
   */
  constructor(props) {
    super(props);
    this.state = {
      itemname: '',
    };
  }
  handleInput = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  handleSubmit = (e) => {
    if (this.state.itemname === '') {
      alert('Empty Todo');
      return;
    }
    e.preventDefault();
    this.setState({ itemname: '' });
    const item = { name: this.state.itemname };
    this.props.addTodo(item);
    this.props.history.push('/');
  };
  /**
   * renders Add
   *
   * @return {Element} react element
   */
  render() {
    return (
      <div>
        <form className="addForm">
          <div className="form-group">
            <input
              type="text"
              name="itemname"
              value={this.state.itemname}
              onChange={this.handleInput}
              placeholder="Add a new todo"
            />
          </div>
          <button
            type="button"
            className="btn"
            onClick={() => this.props.history.push('/')}
          >
            Cancel{' '}
          </button>
          <button type="button" className="btn" onClick={this.handleSubmit}>
            Add{' '}
          </button>
        </form>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addTodo: (item) => {
      dispatch(TodoActions.addTodo(item));
    },
  };
};
Add.propTypes = {
  addTodo: PropTypes.func,
  history: PropTypes.object,
};

export default connect(null, mapDispatchToProps)(Add);
