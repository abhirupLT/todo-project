import React from "react";
import "./App.css";
import Home from "./components/Home";
import Add from "./components/Add";
import Completed from "./components/Completed";
import Pincode from "./components/Pincode";
import Navbar from "./components/Navbar";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./Store/store";
import Edit from "./components/Edit";
function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <div className="App">
          <Navbar />
          <div className="App-main">
            <Switch>
              <Route
                exact
                path="/"
                render={(routeProps) => <Home {...routeProps} />}
              />
              <Route
                exact
                path="/add"
                render={(routeProps) => <Add {...routeProps} />}
              />
              <Route exact path="/completed" render={() => <Completed />} />
              <Route
                exact
                path="/edit/:id"
                render={(routeProps) => <Edit {...routeProps} />}
              />
              <Route exact path="/pincode" render={() => <Pincode />} />
            </Switch>
          </div>
        </div>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
